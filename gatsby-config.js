module.exports = {
  siteMetadata: {
    title: `AlioIt`,
    description: `We are a Software Development and IT consulting company that is young compared to some of our competitors. For this reason we also are lean decision makers. The team is lead by senior developers with deep knowledge of the Software Development business and Project Management Skills to help ensure delivery on budget and on schedule.
    `,
    author: `@jonnitg`,
    siteUrl: 'https://alioit.com',
  },
  plugins: [
    `gatsby-plugin-material-ui`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-google-fonts-v2`,
      options: {
        fonts: [
          {
            family: `Roboto`,
            variable: true,
            weights: ['200..900'],
          },
          {
            family: `Barlow`,
            variable: true,
            weights: ['200..900'],
          },
          {
            family: `Futura`,
            variable: true,
            weights: ['200..900'],
          },
          {
            family: `Manrope`,
            variable: true,
            weights: ['200..900'],
          },
        ],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    // Content
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `blog`,
        path: `${__dirname}/src/content/blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `careers`,
        path: `${__dirname}/src/content/careers`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `case-studies`,
        path: `${__dirname}/src/content/case-studies`,
      },
    },
    {
      resolve: `gatsby-plugin-hubspot`,
      options: {
        trackingCode: '5727045',
        respectDNT: true,
        productionOnly: true,
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /(icons\/.*)|(\.inline)\.svg$/,
        },
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `AlioIt Website`,
        short_name: `AlioIt`,
        start_url: `/`,
        background_color: `#11161C`,
        theme_color: `#11161C`,
        display: `minimal-ui`,
        icon: `src/images/alioit/icon-512.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 800,
            },
          },
          {
            resolve: `gatsby-remark-highlight-code`,
          },
          {
            resolve: `gatsby-remark-gifs`,
          },
        ],
      },
    },
    `gatsby-source-instance-name-for-remark`,
    `gatsby-plugin-remove-trailing-slashes`,
  ],
}
