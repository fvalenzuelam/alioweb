---
title: 'Higher Ed SalesForce'
description: 'SalesForce'
order: 4
headline: 'Optimizing Student Relations Management in Higher Ed with Salesforce'
logo: salesforce
stack:
  devices:
    - database
  frameworks:
    - apex
    - visualforce
    - lightning
    - processflowbuilder
    - talend
    - sql
services:
  Services:
    - Software Development (Salesforce)
    - Application maintenance and support
    - Data Migration / ETL
  Sector:
    - Higher Ed SalesForce
  Year:
    - 2021
  Timeline:
    - Ongoing
about: |
  The University of St. Augustine for Health Sciences (USAHS) has implemented Salesforce as its primary student relationship management (CRM) platform. To make sure your Salesforce instance stays up to date and runs effectively, they've hired a team of Salesforce developers and administrators.

  Thanks to the collaboration between the university and the Salesforce development and administration team, USAHS has been able to maintain an up-to-date and effective Salesforce instance that significantly improves student relationship management and other administrative tasks. In addition, the customization of the Salesforce platform has allowed the university to adapt to the changing needs of students and the healthcare market.
galleryImages:
  - ../../images/case-studies/salesforce/gallery-image-4.png
  - ../../images/case-studies/salesforce/gallery-image-5.png
objectives:
  - The team maintains and updates Salesforce to meet the university's and students' needs, following EDA architecture. They also collaborate with university departments to keep integrations with other platforms and tools up-to-date and effective.
  - The university asked for new features and custom tools on Salesforce, such as personalized solutions for application management, student tracking, billing, payment handling, and other aspects of student relationship management.
  - Salesforce's team of developers and administrators work closely with university staff to identify and prioritize these needs and to develop custom solutions that are easy to use and efficient.
challenge: |
  Create a user-friendly solution for staff to quickly find information they need on Salesforce. Consider staff needs and limitations. The goal is to improve user experience and streamline access to important information for admission advisors, enrollment advisors, and administrators. Additionally, modify marketing and lead introduction flows to align with the admission and enrollment process.
---
