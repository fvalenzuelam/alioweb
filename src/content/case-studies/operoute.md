---
title: 'Operoute'
description: 'Manufacturing Application'
order: 3
headline: 'Enabling Industry 4.0 for all'
logo: operoute
stack:
  devices:
    - database
  frameworks:
    - js
    - angular
    - socketIo
services:
  Services:
    - Web Application
    - Mobile Application
    - UX Design
    - UI Design
  Sector:
    - Manufacturing
  Year:
    - 2020
  Timeline:
    - Ongoing
about: |
  Operoute offers a multi-platform connectivity that allows a more efficient data flow that helps decision making by providing real insights to all stakeholders involved in the manufacturing process.
galleryImages:
  - ../../images/case-studies/operoute/gallery-image-1.jpg
  - ../../images/case-studies/operoute/gallery-image-2.jpg
  - ../../images/case-studies/operoute/gallery-image-3.jpg
objectives:
  - Reduce production times and costs, for this it is important to have access to software that allows a correct management and documentation of information.
  - Ensure that work flows are traceable from the receipt of a client purchase order, development designs, work instructions, and the approval of each of the production operations conforming to quality assurance best practices for the industry and keeping a record of all parts and its operations as required by norms such as AS9100.
  - Increase sense of operator empowerment and awareness towards achieving production goals and maintain business alignment with company strategy in terms of productivity, zero defects and low costs.
challenge: |
  Guarantee the correct execution of sops/work instructions. Improve the quality of information fed to the shop floor (supervisors, quality inspectors, etc). Access to real time data such and documentation required for production activities via a modern API and web integrations.
---
