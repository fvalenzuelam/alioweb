---
title: 'Nerito'
description: 'FinTech Application'
order: 2
headline: 'Empowering The People of México'
logo: nerito
stack:
  devices:
    - mobile
    - desktop
  frameworks:
    - js
    - react
    - aws
services:
  Services:
    - Web Application
    - Mobile Application
    - UX Design
    - UI Design
  Sector:
    - Fintech
  Year:
    - 2020
  Timeline:
    - Over
about: |
  The main goal of the company is to become an hybrid ecosystem comprising fintech & commercial services based on that leverages mobile devices and technology such as serverless cloud architecture for higher availability and scalability.

  The audience is non-digital friendly consumers (vs lower and middle class) economic status that usually do not rely much on brick and mortar banking institutions.
galleryImages:
  - ../../images/case-studies/nerito/gallery-image-1.jpg
  - ../../images/case-studies/nerito/gallery-image-2.jpg
  - ../../images/case-studies/nerito/gallery-image-3.jpg
objectives:
  - Provide a technologically advanced platform for providing a finance platform to a mass user audience, primarily the "non" banking friendly users to help streamline financial services in a friendly and useful manner.
  - Build native mobile apps for Merchants and Users that can interact with each other via in app purchasing, deposits and withdrawals in a secure and reliable way for greater adoption in urban and rural areas of Mexico.
  - Help transform a culture of non digital friendly users to a more digital and dynamic culture for a better adoption of technology as a tool and facilitator in the financial space.
challenge: |
  The main goal of the company is to become a financial institution that leverages mobile devices and technology such as serverless cloud architecture for higher availability and scalability.
---
