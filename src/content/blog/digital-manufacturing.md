---
title: Digital Manufacturing at its finest
date: 2018-03-30T15:04:10.000Z
description: Why you should hop on the digital manufacturing bus towards greater productivity/creativity
author: Alfonso Bonillas
topic: MRP, OPTIS
---

![digital mfg](../../images/blog/digital-manufacturing/digimanu.gif)

Just like brain connections that allows us to make better decisions, it gives us alerts when some part of our body needs attention, those connections that let us to control our muscles and transform them in art like running or a ballet routine. The perfect connection between data, real manufacturing process and analysis.

## What digital manufacturing is

Digital manufacturing is really about leveraging real time data to improve decision making and better monitoring of improvement efforts your teams have implemented for the shop floor. It also allows for your different production and quality management systems to be more effective by allowing user and automated input for

- work orders
- routers (route cards)
- production planning
- logistics (internal and external)
- non conformance part tracking
- ad hoc reporting / KPIs

Technology and digitization in industry represents an essential incentive towards achieving an increase in the ability of an enterprise to play an important role in a global market. To be able to identify specific areas of opportunity in a real time or Dynamic manner is of great importance when working on process improvements. Digitization represents a real step change these days creating new possibilities for growth and development in your enterprise.
