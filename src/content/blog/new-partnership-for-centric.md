---
title: New partnership for Centric Mining System in México
date: 2019-10-8T07:04:10.000Z
description: New partnership for Centric Mining Systems in Mexico means enhanced support for Centric clients.
author: Alfonso Bonillas
topic: Centric Mining Systems
---

# Sudbury, Ontario. October 8, 2019 –

Centric Mining Systems (CMS) is pleased to announce a strategic partnership with Alio IT Solutions located in Hermosillo, Mexico. This newly formed partnership will further enhance support for current and future Centric customers in Mexico.

Alio IT is now a value-added partner for CMS focused on selling and supporting the Centric mine information management system in Mexico. Alio IT Solutions is well positioned to deliver value to Centric clients as a technology driven organization with mining industry experience and a focus on productivity improvement.

Centric has been in operation for over 20 years and has established itself as an industry leader in digital mine transformation. Centric’s software is used at mine sites worldwide allowing key decision makers to make timely decisions. From data capture to data warehousing, inventory management, reconciliation, business intelligence and reporting – Centric provides a clear view of your entire operations.

In a similar approach to Centric Mining Systems, Alio IT Solutions drives increased productivity for mining companies by gaining more control and visibility of their operations. The team is led by mining professionals with deep knowledge of software development and implementation as well as project management, ensuring quality project delivery on budget and schedule.

Alio IT Solution’s CEO, Alfonso Bonillas will make a presentation during the upcoming XXXIII International Mining Convention in Acapulco Mexico on October 23rd at 11am (Sala San Martin), providing more information on mine intelligence software and applications. The International Mining Convention, which is one of the most important mining events in Mexico, takes place from October 22 – 25 2019 at the Hotel Princess Mundo Imperial.

What was very interesting is how Google in particular has layed out the structure for developers to build these apps or agents as they call them.

**Inquiries: Meghan Mathe**
**Marketing and Communications Manager**
**+1 705.670.8922**
**email: info@centricminingsystems.com**
**www.centricminingsystems.com**
