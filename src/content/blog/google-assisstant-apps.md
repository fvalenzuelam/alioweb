---
title: Creating a Google Assistant App
date: 2019-07-24T07:04:10.000Z
description: Building swag AI applications or agents for all
author: Alfonso Bonillas
topic: Google Assistant
---

# Creating a Google Assistant App - Experience

We know that AI has been becoming main stream for a few years now and its now even more with the advent of amazing AI tools to enable human - machine interaction to do things maybe 10 years ago seemed farther away from becoming present. The first probably most popular tool or software is **SIRI** for Apple which most IPhone users are using on a regular basis. Now both Google and Amazon have their flavor of AI also readily available for Software developers out there to use for their own goals/interests. I am refering to Amazon Alexa (Lex) [Lex](https://https://aws.amazon.com/lex/) and Google Assistant [GA](https://assistant.google.com/).

We had the pleasure of developing a POC last year with **Google Assistant** to connect to an API and retrieve information from traffic light availability and performance among other things. This was a very cool project where a person could ask the GA (Google Assistant) agent several questions and would get a read out performance report, as if you were talking to an actual person with a lot of knowledge, **which is ultimately the goal of these AI enabled applications**.

What was very interesting is how Google in particular has layed out the structure for developers to build these apps or agents as they call them.

##### The main components of the agent consist of:

- Actions. An action is an entry point with the interaction that is created in the
  assistant. Users can request the action, either by written or verbal command.
  To start a conversation, the user must invoke an action using the wizard. To the
  invoke any phrase that matches a command, say, “Hey google, talk to
  AGENT NAME. ” This tells the assistant what action to call.
  From this point forward, the user directs directly to the action. This conversation
  it continues as a talk in both directions, that is, both respond to the dialogue of the another until the user's attempt is completed, and the conversation is terminated.
- Intents. It refers to a goal or task that you want to perform. You must respond to a Unique identifier that corresponds to what the user said. An explanation of how it works: When the user requests an action, say, “Google, talk to AGENT NAME”, this user entry will invoke the action of “AGENT NAME. " This action responds with a conversation response, it will be communicated to the user and that request will end, with an answer like “AGENT NAME here! What can I do for you? ”, that being our answer, the action goes on to finish its work.

For each action, responses must be delivered in such a way that the intent or entry of the user satisfies the information that our action requires to finish.

In intents there is what is known as parameters, which represent the values that
must be extracted from the user's phrases, for example, your attempt awaits an address, and the user response is “I want the first street and L Avenue, number 143, yesterday at 11 ”, your parameters to extract could be the address“ First Street and Avenue L ”, number "143", when "yesterday", and hour "11". That feed the action.

**Please contact us if you are looking to integrate AI (agents) into your solutions.**
