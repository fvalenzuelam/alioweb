---
title: Real Time Manufacturing Dashboard
date: 2018-07-28T13:05:00.000Z
description: Offers a multiplatform connectivity tool that allows a more efficient data flow that helps decision making by providing real insights.
author: Alfonso Bonillas
topic: MRP, OPTIS
---

Our flagship product OPTIS will help you take control of your operation and advise you when something is not according to your plan so you can correct and improve faster than ever saving you money and time.

![digital mfg](../../images/blog/real-time-manufacturing-dashboard/alioit-real-time-manufacturing-dashboard.jpeg)
