---
title: Sharing hooks using React context (Providers)
date: 2020-05-12T19:22:00.000Z
description: The big question, how to share states between react components ?
author: Victor Lizarraga
topic: React
---

### Sharing hooks using React context (Providers)

#### The big question: how to share states between react components ?

In react we count with multiple libraries to share states between components, for example redux or mobx but personally I think currently we could share our react state using elements from the react core library.

#### Provider pattern

The provider pattern has tow principal actors (Providers and Consumers) where Provider is in charge of export data such as states or functions and consumers are components that can use them.
![Diagram](../../images/blog/sharing-hooks/diagram.png)

#### Creating our provider

For this example we are going to simulate an auth app where we have our **login view** that contains a form for the login proccess and our **home view** that is going to show the username.
So...
First of all, we need a share link that exports the functions and properties to manage the state

**providers/use-auth.js**
![useAuth](../../images/blog/sharing-hooks/use-auth.png)
Using it directly on our components would be useless because it would init each time we call it, to solve that, we let's to create a context that allows us to share that hook between components

And here is, the first function is the component to init our hook and the second one is the hook to use it wherever we want

**providers/index.js**
![provider](../../images/blog/sharing-hooks/provider.png)

#### Implementation

As we said before the goal is to simulate an auth app with two views the **login view** to input the login data and call the login proccess and the **home view** where we will display the username

Before use our context we have to initialize it on our index js by wrapping our app with the AuthProvider and then we can use our provider wherever we want.

**index.js**
![implementation](../../images/blog/sharing-hooks/provider-implementation.png)

Now In our app JS we are going to use our provider and check if the user is already authenticated and we will display the **home** or **login** view despending of the result of the property `isAuth` which we declared before.

**app.js**
![index](../../images/blog/sharing-hooks/share-index.png)

then in our **login view** we are going to make the form and call the login function from our auth provider to run the process to authenticate the user.

**containers/login.js**
![login](../../images/blog/sharing-hooks/share-login.png)

And finally, thanks to the fact that we can use the authentication provider wherever we want, we can use it in our home view to display the current username and we could even call the logout process and more, depending on how we made our provider.

**containers/home.js**
![login](../../images/blog/sharing-hooks/share-home.png)

#### An easier way to make providers

Instead of create the context and the useprovider manually we could also use the package [react-provider-maker](https://www.npmjs.com/package/react-provider-maker)

```
  $ npm install react-provider-maker
```

And now we can make our provider only with the hook we want to share
![login](../../images/blog/sharing-hooks/share-make-provider.png)

**And thats it !**
