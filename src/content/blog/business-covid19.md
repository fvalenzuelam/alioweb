---
title: Business during and post COVID-19
date: 2020-05-05T10:30:00.000Z
description: Business during and post COVID-19, what to expect and where can we move
author: Alfonso Bonillas
topic: Digital business
---

Digital business shall thrive and prevail during COVID-19 and post COVID-19 for many reasons.

Being physically present is no longer required:

1. Banking
1. Universities and Schools
1. Streaming ( concerts, games, etc ) - Netflix/AppleTV/Youtube/HBO/DisneyPlus, not just them anymore
1. Manufacturing? ( More businesses will strive to join )

We have to be prepared for this change, not just as service providers or product developers but also as consumers of these services. How can this really be our new way of life?

1. Infrastructure investments / expansion (Mexico)
1. Mindset, we are social creatures but will have to get used to not having to be present for EVERYTHING and maybe use your presence for other things? Family, Self-Improvement, Social Work.
1. Be very conscious of new safety/health measures, we are reminded that we are not the supreme being in this planet, its actually the microscopic beings that reign! (Viruses, bacterias, our own blood cells) They can give and take life away from us.

We are Alio IT: (www.alioit.com) and we have worked with:

1. Insuretech
2. Manufacturing
3. Streaming
4. Remote monitoring services
5. Online Universities
6. Cybersecurity

_WE_ need to make sure professionals are better prepared, not just technically and understand fundamentals of writing software but communication skills are _CRITICAL_.

![Dinosaurious](../../images/blog/business-covid19/business-during-and-post-covid-19-2x.jpg)

**Closing**:
No option but to embrace this change and for those of us who are fortunate to have started working in the enhancement of digital businesses to continue focusing on this but also never lose sight of how to help humanity for our future generations sake!
