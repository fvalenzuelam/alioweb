---
title: OPTIS (Online Production Tracking System)
date: 2018-03-30T15:04:10.000Z
description: Industry and Technology and what we are striving to achieve
author: Alfonso Bonillas
topic: MRP, OPTIS
---

![OPTIS](../../images/blog/introduction-to-optis/routecard.jpg)

The use of new technologies for automation and digitization allows for a better management of your enterprise resources facilitating access to:

1. Real Time Production Data pertaining to your Assets.
2. Multiplatform (mobile, web) & Accessible via API.
3. Collaborator and User Data Involved in your Process.

## Data availability and no longer operating in Silos

This is why we built OPTIS, a web based software package that delivers and complies with your data demands for an exceptional managements of your shop floor.

Automation of processes allows greater precision, reduces production times and costs, for this it is important to have access to software that allows a correct management and documentation of information; OPTIS (Online Production Tracking System) offers a multi-platform connectivity that allows a more efficient data flow that helps decision making by providing real insights.

There is a major improvement in the quality of information through the use of OPTS thanks to biometric and facial recognition integration; its adaptability to each enterprise´s needs allows also for flexibility and reliability and ensures all users involved in your manufacturing process are connected through real time information.

We ensure that work flows are traceable from the receipt of a client purchase order, development designs,
work instructions, and the approval of each of the production operations conforming to quality assurance
best practices for the industry and keeping a record of all parts and its operations as required by norms
such as AS9100.

OPTIS will help you with the following key aspects:

1. GUARANTEE THE CORRECT EXECUTION OF SOPS/WORK INSTRUCTIONS.
2. IMPROVE THE QUALITY OF INFORMATION FED TO THE SHOP FLOOR (SUPERVISORS, QUALITY INSPECTORS, ETC).
3. ACCESS TO REAL TIME DATA SUCH AND DOCUMENTATION REQUIRED FOR PRODUCTION ACTIVITIES
4. MODERN API AND WEB INTEGRATIONS.

Other key features you can take advantage of as a client of ours are INCREASE SENSE OF OPERATOR EMPOWERMENT AND AWARENESS TOWARDS ACHIEVING PRODUCTION GOALS. Last but not least this applications runs in the cloud so you no longer have to worry about hardware set up / back ups/ availability (its covered).
