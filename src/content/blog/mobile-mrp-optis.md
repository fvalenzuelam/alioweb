---
title: Mobile Production Tracking System
date: 2018-11-06T15:04:10.000Z
description: A new dawn in MRP and MES software
author: Alfonso Bonillas
topic: MRP, OPTIS
---

![OPTIS_Mobile](../../images/blog/mobile-mrp-optis/optis-login.png)

Get to know our new mobile version, runs on Android and iOS thanks to the slick tech used to build it.

## Have your operators actively collaborating

1. GUARANTEE THE CORRECT EXECUTION OF SOPS/WORK INSTRUCTIONS.
2. IMPROVE THE QUALITY OF INFORMATION FED TO THE SHOP FLOOR (SUPERVISORS, QUALITY INSPECTORS, ETC).
3. ACCESS TO REAL TIME DATA SUCH AND DOCUMENTATION REQUIRED FOR PRODUCTION ACTIVITIES
4. MAKE WORK ENJOYABLE FOR OPERATORS AGAIN

## Feel free to contact us for more information

![OPTIS_Mobile](../../images/blog/mobile-mrp-optis/optis-workorder-selected-detail.png)
![OPTIS_Mobile](../../images/blog/mobile-mrp-optis/optis-operation-pause-serial-saved.png)

Thanks!
