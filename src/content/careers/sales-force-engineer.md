---
title: SALES FORCE ENGINEER
date: 2020-05-21T19:58:46.804Z
summary: |
  Salesforce developers analyze company processes, developing CRM workflows and custom solutions for specific business needs. Salesforce developers work collaboratively with others on the team to create custom, scalable processes for the company
---

#### Sales Force Job Summary:

Salesforce developers analyze company processes, developing CRM workflows and custom solutions for specific business needs. Salesforce developers work collaboratively with others on the team to create custom, scalable processes for the company

#### Job Responsibilities:

- Develop customized solutions within the Salesforce platform to support critical business functions and meet project objectives, client requirements and company goals
- Manage daily support and maintenance of internal Salesforce instance, and conduct long-term improvement operations to ensure compatibility with evolving mission requirements
- Communicate with project managers, clients and other developers to design cohesive project strategies and ensure effective collaboration throughout all phases of development, testing and deployment
- Maintain a flexible and proactive work environment to facilitate a quick response to changing project requirements and customer objectives, and innovate ways to meet mission goals successfully
- Interact directly with clients, managers and end users as necessary to analyze project objectives and capability requirements, including specifications for user interfaces, customized applications and interactions with internal Salesforce instances
- Provide system administration support of internal and customer-facing Salesforce environment, especially related to customized applications, user permissions, security settings, custom objects and workflow
- Collaborate with various internal departments, including marketing, product development and operations, to ensure Salesforce environment supports internal needs relating to functionality and performance

#### Job Skills & Qualifications:

- Bachelor’s degree in computer science
- Minimum work experience: eight years of software development and four years of Salesforce application development
- Self-motivated and creative

Preferred:

- Salesforce Developer Certification ( Optional but nice to have)
- Experience developing customer-facing user interfaces

**ENGLISH FLUENCY**

[→ Download Job Description ▼](https://drive.google.com/file/d/13Yc-Id3CiHVJemFIRlfbtMszIuFnv6Eg/view?usp=sharing)
