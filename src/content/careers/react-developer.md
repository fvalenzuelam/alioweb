---
title: REACT DEVELOPER
date: 2019-10-14T10:28:10.000Z
summary: |
  Creates user information solutions by developing, implementing, and maintaining React based components and interfaces.
---

##### REACT Developer Job Responsibilities:

Creates user information solutions by developing, implementing, and maintaining React based components and interfaces.

##### REACT Developer Job Duties:

- You love finding solutions to interesting problems
- Defines objectives by analyzing user requirements; envisioning system features and functionality.
- Enhances organization reputation by accepting ownership for accomplishing new and different requests; exploring opportunities to add value to job accomplishments.
- Implement and iterate quickly on concepts, prototypes and production-ready software.
- Be a positive influence, respect your peers and adapt to cultural trends and activities.

##### REACT Developer Skills and Qualifications:

React/Redux (Javascript), HTML5/CSS3, Verbal Communication, Software Debugging/Unit Testing, AWS tools: Route53, CloudFront, API Gateway, Lambda, DynamoDB, S3,English: Interact with clients who speak English

[→ Download Job Description ▼](https://drive.google.com/file/d/1QvRk4SK7LYPJcqrN_F0M4_A7BRtCPQjo/view?usp=sharing)
