---
title: NODE JS DEVELOPER
date: 2020-05-21T19:58:46.804Z
summary: |
  Creates user information solutions by developing,
  implementing, and maintaining Node based API's.
---

#### NODE Developer Job Responsibilities:

Creates user information solutions by developing,
implementing, and maintaining Node based API's.

#### NODE Developer Job Duties

- You love finding solutions to interesting problems
- Defines objectives by analyzing user requirements; envisioning system features and functionality.
- Enhances organization reputation by accepting ownership for accomplishing new and different requests; exploring opportunities to add value to job accomplishments.
- Implement and iterate quickly on concepts, prototypes and production-ready software.
- Be a positive influence, respect your peers and adapt to cultural trends and activities.

#### NODE Developer Skills and Qualifications

Node JS, Javascript, AWS Lambda, TDD (Test Driven
Development) [Rich Domain Models], Repository Pattern, 3-tier
architecture (view/presentation layer, logic layer, data layer),
Software Debugging/Unit Testing, DocumentDB (MongoDB),
DynamoDB, KOA/Express

**English: Interact with clients who speak English**

[→ Download Job Description ▼](https://drive.google.com/file/d/1eLMPDYPEVNBdmdHUv0v_bfJ-edH_hE_p/view?usp=sharing)
