---
title: DEVOPS ENGINEER
date: 2020-05-21T19:58:46.804Z
summary: |
  We are looking for an experienced DevOps (Development
  and Operations) professional to join our growing
  organization. In this position, you will manage infrastructure
  projects and processes. A keen attention to detail,
  problem-solving abilities, and solid knowledge base are
  essential
---

#### DevOps Job Summary:

We are looking for an experienced DevOps (Development
and Operations) professional to join our growing
organization. In this position, you will manage infrastructure
projects and processes. A keen attention to detail,
problem-solving abilities, and solid knowledge base are
essential

#### DevOps Duties and Responsibilities

- Work directly with the DevOps team and Senior DevOps manager to test system integrity
- Design and implement build, deployment, and configuration management
- Test implemented designs
- Build and test automation tools for infrastructure provisioning
- Handle code deployments in all environments
- Monitor metrics and develop ways to improve
- Provide technical guidance and educate team members and coworkers on development and operations
- Brainstorm for new ideas and ways to improvement development delivery
- Consult with peers for feedback during testing stages
- Build, maintain, and monitor configuration standards
- Maintain day-to-day management and administration of projects
- Manage CI and CD tools with team
- Document and design various processes; update existing processes
- Improve infrastructure development and application development
- Follow all best practices and procedures as established by company

#### DevOps Requirements and Qualifications

- High school degree or equivalent; bachelor’s degree in CS, engineering, software engineering, or related field
- Two years previous experience in development and operations, or related IT, computer, or operations field
- Previous experience with software development, infrastructure development, or development and operations using GCP and Kubernetes.
- Experience with Linux infrastructures, NGINX, CI/CD tools, scripting such as JavaScript, Skipper experience nice to have, Scrum/Kanban Agile workflow methodologies
- Up-to-date on latest industry trends; able to articulate trends and potential clearly and confidently
- Good interpersonal skills and communication with all levels of management
- Able to multitask, prioritize, and manage time efficiently

**ENGLISH FLUENCY**

[→ Download Job Description ▼](https://drive.google.com/file/d/1McYQgtgG1805m0mIhiYCFu2oi7IvulNK/view)
