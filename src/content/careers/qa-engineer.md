---
title: TEST AUTOMATION DEVELOPER
date: 2020-05-21T19:58:46.804Z
summary: |
  Creates testing solutions by developing, implementing, and maintaining JEST and Selenium based scripts.
---

#### Test Automation Developer Job Responsibilities:

Creates testing solutions by developing, implementing, and maintaining JEST and Selenium based scripts.

#### Test Automation Developer Job Duties:

- You love finding solutions to interesting problems
- Defines objectives by analyzing user requirements; envisioning system features and functionality.
- Enhances organization reputation by accepting ownership for accomplishing new and different requests; exploring opportunities to add value to job accomplishments.
- Implement and iterate quickly on concepts, prototypes and production-ready software.
- Be a positive influence, respect your peers and adapt to cultural trends and activities.

#### Test Automation Developer Skills and Qualifications:

React JS/Javascript, JEST, Selenium, [Rich Domain Models],
Repository Pattern, 3-tier architecture (view/presentation layer,
logic layer, data layer), Software Debugging/Unit Testing,
DocumentDB (MongoDB), DynamoDB, KOA/Express

**English: Interact with clients who speak English**

[→ Download Job Description ▼](https://drive.google.com/file/d/1v-tkpVESp39yf3gTXUevxWZ6QzcD23db/view?usp=sharing)
