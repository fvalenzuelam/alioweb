import React from 'react'
import { graphql } from 'gatsby'
import styled from '@emotion/styled'
import { HeadingContainer, Text } from '../components/common/typography'
import AppLayout from '../components/layouts/appLayout'
import GatsbyImage from 'gatsby-image'
import { BLACK_COLOR_RGB, PRIMARY_COLOR } from '../components/common/color'
import bp from '../components/common/breakpoints'
import SEO from '../components/seo'

const Container = styled.div`
  display: grid;
  grid-template-areas:
    'image'
    'headings'
    'text';
  grid-template-columns: 1fr;
  height: 100%;
  padding-bottom: 2rem;

  ${bp[1]} {
    grid-template-columns: 1fr 1fr;
    grid-template-areas:
      'image image'
      'headings text';
  }

  ${bp[2]} {
    grid-template-columns: 3fr 2fr;
  }
`

const ImageContainer = styled.div`
  grid-area: image;
`

const HeadingsContainer = styled.div`
  grid-area: headings;
  justify-self: start;
  align-self: center;
  padding: 1rem 2rem;
  width: 100%;
  box-sizing: border-box;

  ${bp[1]} {
    width: initial;
    justify-self: center;
  }
`

const TextContainer = styled.div`
  grid-area: text;
  justify-self: center;
  align-self: center;
  padding: 1rem 2rem;
  box-sizing: border-box;
  line-height: 1.3;
`

const Heading = styled.h3`
  font-size: 1.5rem;
  font-weight: 700;
  font-family: var(--secondary-font);
  text-transform: uppercase;
`

const SubHeading = styled.h4`
  font-size: 1.5rem;
  font-weight: 700;
  font-family: var(--secondary-font);
  color: ${PRIMARY_COLOR};
  margin-bottom: 0;

  ${bp[1]} {
    margin: initial;
  }
`

const TextHeading = styled.h5`
  font-size: 1.5rem;
  font-weight: 700;
  font-family: var(--secondary-font);
  color: ${BLACK_COLOR_RGB(0.4)};
  margin: 0 0 0.5rem;

  ${bp[1]} {
    margin: 1rem 0;
  }
`

const TeamsPage = ({ data }) => {
  return (
    <AppLayout>
      <SEO title="Teams" canonical="Teams" description="Remote software teams, Nearshore development teams, Staff augmentation" />
      <HeadingContainer
        title="Your remote team, but... Closer"
        subtitle="Our DNA"
      />
      <Container>
        <ImageContainer>
          <GatsbyImage fluid={data.teamsBackground.fluid} />
        </ImageContainer>

        <HeadingsContainer>
          <Heading>What We're made of</Heading>
          <SubHeading>
            Committed to Quality and Focused on true Needs
          </SubHeading>
        </HeadingsContainer>
        <TextContainer>
          <TextHeading>We believe...</TextHeading>
          <Text>In agile development.</Text>
          <Text> That the adaptive capacity makes us stronger.</Text>
          <Text>
            In finding solutions thinking out of the box with our feet on the
            ground.
          </Text>
          <Text>
            Everything is possible, but not just from working hard, but thinking
            and being disciplined/focused.
          </Text>
          <Text>In passion for coding and having fun while doing it.</Text>
        </TextContainer>
      </Container>
    </AppLayout>
  )
}

export const query = graphql`
  query {
    teamsBackground: imageSharp(
      fluid: { originalName: { eq: "teams-background.jpg" } }
    ) {
      fluid(jpegQuality: 100, quality: 100, maxWidth: 1200, maxHeight: 520) {
        ...GatsbyImageSharpFluid
      }
    }
  }
`

export default TeamsPage
