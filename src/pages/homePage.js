import React, { useCallback } from 'react'
import styled from '@emotion/styled'
import AppLayout from '../components/layouts/appLayout'
import SEO from '../components/seo'
import ContactForm from '../components/contact-form'
import AlioVision from '../components/alioVision'
import Hero from '../components/hero'
import ServicesLanding from '../components/servicesLanding'
import bp from '../components/common/breakpoints'
import NftInfo from '../components/nftInfo'

const Content = styled.div`
  z-index: 0;
  height: 100%;
  display: flex;
  flex-direction: column;
  gap: 10vh;
  &:focus {
    outline:none;
  }

`
const SectionContainer = styled.div`
  height: 100%;  
  width: 100%;
  ${bp[3]} {
    // height: calc(100vh - 116px);
  }
`


const HomePage = () => {
  const divInput = useCallback((div) => {
    if (div) {
      div.focus();
    }
  }, []);

  return (
    <AppLayout>
      <SEO title="Home" canonical="Home" />
      <Content tabIndex='-1' ref={divInput}>
        <SectionContainer>
          <Hero />
        </SectionContainer>
        <SectionContainer>
          <ServicesLanding />
        </SectionContainer>
        <SectionContainer>
          <NftInfo />
        </SectionContainer>
        <SectionContainer>
          <AlioVision />
        </SectionContainer>
        <SectionContainer>
          <ContactForm />
        </SectionContainer>
      </Content>
    </AppLayout>
  )
}


export default HomePage