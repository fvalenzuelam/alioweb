import React, { useContext } from 'react'
import GoogleAuthorize from 'react-google-authorize'
import { graphql, navigate } from 'gatsby'
import styled from '@emotion/styled'
import Image from 'gatsby-image'
import GlobalLayout from '../../components/layouts/global'
import { AuthContext, signInAction } from '../../context/auth-context'

const GoogleButton = styled.button`
  background-color: transparent;
  border: 0;
  cursor: pointer;
`

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
`

const AdminPage = ({ data }) => {
  const { state, dispatch } = useContext(AuthContext)

  const getGoogleUser = async (response) => {
    const request = await fetch(
      `https://www.googleapis.com/oauth2/v3/userinfo?access_token=${response.access_token}`,
      { headers: { accept: 'application/json, text/plain, */*' } }
    )
    const payload = await request.json()
    if (payload.hd === 'alioit.com') {
      dispatch(signInAction(payload))
    } else {
      console.error('error on login')
    }
  }

  if (state.isLoggedIn && state.postType === 'blog') {
    navigate('/admin/blog')
    return null
  } else if (state.isLoggedIn && state.postType === 'career') {
    navigate('/admin/career')
    return null
  }

  return (
    <GlobalLayout>
      <Container>
        <GoogleAuthorize
          clientId="212251758525-mnr1navr9p26lipfrt56b22asd15r106.apps.googleusercontent.com"
          buttonText="Authorize"
          hostedDomain="alioit.com"
          onSuccess={getGoogleUser}
          onFailure={(e) => null}
          render={(renderProps) => (
            <GoogleButton onClick={renderProps.onClick}>
              <Image fixed={data.googleButton.fixed} />
            </GoogleButton>
          )}
        />
      </Container>
    </GlobalLayout>
  )
}

export const query = graphql`
  query {
    googleButton: imageSharp(
      fixed: { originalName: { eq: "google-signin.png" } }
    ) {
      fixed(jpegQuality: 100, quality: 100, width: 160) {
        ...GatsbyImageSharpFixed
      }
    }
  }
`

export default AdminPage
