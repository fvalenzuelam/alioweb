import React, { useEffect, useState, useCallback, useContext } from 'react'
import moment from 'moment'
import { graphql, Link, navigate } from 'gatsby'
import styled from '@emotion/styled'
import { HeadingContainer, Text } from '../../components/common/typography'
import AppLayout from '../../components/layouts/appLayout'
import GatsbyImage from 'gatsby-image'
import { BLACK_COLOR_RGB, PRIMARY_COLOR, BLACK_COLOR, WHITE_COLOR, WHITE_COLOR_RGB } from '../../components/common/color'
import bp from '../../components/common/breakpoints'
import { getBlogPosts } from '../../utils/requests'
import SEO from '../../components/seo'
import Select from '../../components/select'
import Button from '@material-ui/core/Button'
import { AuthContext, postTypeAction } from '../../context/auth-context'
import { motion } from 'framer-motion'

const Content = styled.div`
  position: relative;
  padding-top: 2rem;
`
const BackgroundIMG = styled.div`
  display: none;
  ${bp[2]}{
    display: unset;
    height:380px;
    position: absolute;
    top: 14vh;
    left: 0;
    width: 100%;
    height: 66vh;
  }
`

const BackgroundFixed = styled.div`
  ${bp[2]}{
    height:380px;
    position: fixed;
    width: 100%;
    height: 66vh;
  }
`

const ActionPanel = styled.div`
  display: flex;
  flex-direction: column;
`
const PostsContainer = styled(motion.div)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  row-gap: 1rem;
  column-gap: 1rem;
  width: 100%;
  margin: 3rem auto 0;
  padding-bottom: 3rem;
  z-index: 999;
  ${bp[1]} {
    width: 100%;
    grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
    max-width: 660px;
  }

  ${bp[2]} {
    grid-template-columns: repeat(auto-fill, minmax(320px, 1fr));
    max-width: 860px;
  }

  ${bp[3]} {
    max-width: 1080px;

  }
`
const PostCard = styled.div`
  display: grid;
  grid-template-rows: auto auto 1fr auto;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  padding: 2rem;
  border-radius: 4px;
  background: white;
  height: 280px;
  box-sizing: border-box;
  // background: rgba(255, 255, 255, 0.7);
  // backdrop-filter: blur(40px);
  background: ${BLACK_COLOR};
  z-index:0;
`
const PostTitle = styled.h4`
  font-weight: bold;
  font-family: 'Manrope';
  font-size: 1rem;
  text-transform: uppercase;
  margin-top: 0;
  margin-bottom: 1rem;
  color: ${WHITE_COLOR};
`
const PostSummary = styled(Text)`
  color: ${WHITE_COLOR_RGB(0.4)};
  font-weight: 400;
  overflow: hidden;
  margin-bottom: 1rem;
  font-family: 'Manrope';
  font-size: 1rem;

`
const PostDetailsContainer = styled.div`
  display: grid;
  grid-auto-flow: column;
`
const PostDate = styled.span`
  color: ${WHITE_COLOR};
  font-family: 'Source Code Pro';
  font-size: 12px;
`
const PostAuthor = styled.div`
  color: ${WHITE_COLOR_RGB(0.5)};
  font-size: 12px;
  margin-bottom: 8px;
  font-weight: 800;
`

const PostMoreInfoLink = styled(Link)`
  color: ${PRIMARY_COLOR};
  justify-self: end;
  font-weight: 600;
  font-size: 0.75rem;
`
const SelectContainer = styled.div`
  display: flex;
  padding-top: 3rem;
  padding-bottom: 1rem;
  gap: 2rem;
  justify-content: center;
`

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
`

const BlogPage = ({ data }) => {
  const { backgroundDesktop, backgroundMobile, allBlogPosts } = data
  const [blogPosts, setBlogPosts] = useState([])
  const [filter, setFilter] = useState('')
  const [authorFilter, setAuthorFilter] = useState(null)
  const { dispatch } = useContext(AuthContext)

  const createBlog = () => {
    navigate('/admin')
    dispatch(postTypeAction('blog'))
  }

  const normalizeMarkdownPosts = (mdPosts) => {
    return mdPosts.map(({ node: post }) => ({
      ...post.frontmatter,
      id: post.id,
      slug: `/blog/${post.parent.name}`,
    }))
  }

  const normalizeAwsPosts = (awsPosts) => {
    return awsPosts.map((post) => ({
      ...post,
      mainDate: post.date,
      date: moment(post.date).format('MMMM Do, YYYY'),
      slug: `/blog/${post.id}`,
    }))
  }

  const normalizePosts = useCallback((awsPosts, mdPosts) => {
    const allPosts = [
      ...normalizeAwsPosts(awsPosts),
      ...normalizeMarkdownPosts(mdPosts),
    ]

    allPosts.sort((a, b) => {
      if (a?.mainDate && b?.mainDate) {
        return new Date(b.mainDate).getTime() - new Date(a.mainDate).getTime()
      }

      return 0
    })

    setBlogPosts(allPosts)
  }, [])

  useEffect(() => {
    const fetchAwsPosts = async () => {
      const { data: awsPosts } = await getBlogPosts('blog')
      normalizePosts(awsPosts, allBlogPosts.edges)
    }

    fetchAwsPosts()
  }, [allBlogPosts.edges, normalizePosts])

  const postsFilter =
    filter && authorFilter
      ? blogPosts.filter((item) => {
        return item.author === filter
      })
      : blogPosts.filter((item) => {
        return item.topic === filter
      })

  const sources = [
    backgroundMobile.fluid,
    {
      ...backgroundDesktop.fluid,
      media: `(min-width: 768px)`,

    },
  ]
  return (
    <AppLayout>
      <SEO title="Blog" canonical="blog" />
      <Content>
        <HeadingContainer
          icon={'arrow-left'}
          style={{ justifyContent: 'center' }}
          title="The Software Developer's Blog"
        />
        <BackgroundIMG>
          <BackgroundFixed>
            <GatsbyImage
              //fluid={background.fluid}
              fluid={sources}
              style={{ borderRadius: '4px', opacity: '0.9' }}
            />
          </BackgroundFixed>
        </BackgroundIMG>
        <ActionPanel>
          <SelectContainer>
            <Select
              blogs={blogPosts}
              title={'Author'}
              setFilter={setFilter}
              setAuthorFilter={setAuthorFilter}
            />
            <Select
              blogs={blogPosts}
              title={'Topic'}
              setFilter={setFilter}
              setAuthorFilter={setAuthorFilter}
            />
          </SelectContainer>
          <ButtonContainer>
            <Button
              variant="contained"
              style={{
                backgroundColor: PRIMARY_COLOR,
                color: 'white',
                fontWeight: 'bold',
                fontFamily: 'Source Code Pro',
              }}
              onClick={createBlog}
            >
              Create Post
            </Button>
          </ButtonContainer>
        </ActionPanel>
        <PostsContainer
          transition={{
            delay: 1,
            type: 'spring',
            ease: 'easeOut',
            duration: 2,
          }}
          initial={{ y: 20, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
        >
          {!filter &&
            blogPosts.length > 0 &&
            blogPosts.map((post) => (
              <PostCard key={post.id}>
                <PostAuthor>{post.author}</PostAuthor>
                <PostTitle>{post.title}</PostTitle>
                <PostSummary>{post.description}</PostSummary>
                <PostDetailsContainer>
                  <PostDate>{post.date}</PostDate>
                  <PostMoreInfoLink to={post.slug}>
                    Keep Reading
                  </PostMoreInfoLink>
                </PostDetailsContainer>
              </PostCard>
            ))}
          {filter &&
            postsFilter.length > 0 &&
            postsFilter.map((post) => (
              <PostCard key={post.id}>
                <PostAuthor>{post.author}</PostAuthor>
                <PostTitle>{post.title}</PostTitle>
                <PostSummary>{post.description}</PostSummary>
                <PostDetailsContainer>
                  <PostDate>{post.date}</PostDate>
                  <PostMoreInfoLink to={post.slug}>
                    Keep Reading
                  </PostMoreInfoLink>
                </PostDetailsContainer>
              </PostCard>
            ))}
        </PostsContainer>
      </Content>
    </AppLayout>
  )
}

export const query = graphql`
  query {
    backgroundMobile: imageSharp(
      fluid: { originalName: { eq: "Rectangle-65.jpg" } }
    ) {
      fluid(jpegQuality: 100, quality: 100 , maxWidth: 768, maxHeight:768) {
        ...GatsbyImageSharpFluid
      }
    }
    backgroundDesktop: imageSharp(
      fluid: { originalName: { eq: "blog-background.jpg" } }
    ) {
      fluid(jpegQuality: 100, quality: 100, maxWidth: 1400, maxHeight: 320) {
        ...GatsbyImageSharpFluid
      }
    }
    allBlogPosts: allMarkdownRemark(
      filter: { fields: { sourceInstanceName: { eq: "blog" } } }
      sort: { order: DESC, fields: frontmatter___date }
    ) {
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "MMMM Do, YYYY")
            description
            author
          }
          parent {
            ... on File {
              id
              name
            }
          }
        }
      }
    }
  }
`

export default BlogPage
