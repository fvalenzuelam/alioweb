import React from 'react'
import { graphql } from 'gatsby'
import { useMediaQuery } from '@react-hook/media-query'
import styled from '@emotion/styled'
import { HeadingContainer, Text } from '../components/common/typography'
import AppLayout from '../components/layouts/appLayout'
import { BLACK_COLOR_RGB, WHITE_COLOR } from '../components/common/color'
import bp, { TABLET_PORTRAIT_DEVICE } from '../components/common/breakpoints'

import ReactSVG from '../images/icons/brands/react-logo.svg'
import JavascriptSVG from '../images/icons/brands/js-logo.svg'
import PythonSVG from '../images/icons/brands/python-logo.svg'
import AzureSVG from '../images/icons/brands/azure-logo.svg'
import AwsSVG from '../images/icons/brands/aws-logo.svg'
import SEO from '../components/seo'

const Container = styled.div`
  position: relative;
  display: grid;
  grid-template-rows: 120px 1fr 120px;
  row-gap: 4rem;
  width: 100%;
  min-height: 500px;
  padding: 3rem 3rem;
  background-image: ${(props) => `url(${props.image});`};
  background-repeat: no-repeat;
  background-position: center;
  background-size: 100% 400px;
  overflow: hidden;
  box-sizing: border-box;
`

const TextLeft = styled(Text)`
  align-self: center;
  justify-self: start;
  max-width: 720px;
  color: ${BLACK_COLOR_RGB(0.5)};
`

const TextRight = styled(Text)`
  align-self: center;
  justify-self: flex-end;
  max-width: 720px;
  color: ${BLACK_COLOR_RGB(0.5)};
`

const Showcase = styled.div`
  display: grid;
  align-items: center;
  justify-items: center;
  row-gap: 1rem;
  column-gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
`

const LogoContainer = styled.div`
  display: grid;
  align-items: center;
  justify-content: center;
  width: 200px;
  height: 200px;
  border-radius: 100%;
  background-color: ${WHITE_COLOR};
  box-shadow: 10px 10px 20px ${BLACK_COLOR_RGB(0.1)};
`

const Iframe = styled.iframe`
  display: block;
  overflow: hidden;
  border: none;
  box-sizing: border-box;
`

const VideoContainer = styled.div`
  max-width: 90%;
  margin: 0 auto;

  ${bp[1]} {
    max-width: 80%;
  }
`

const logos = [ReactSVG, JavascriptSVG, PythonSVG, AzureSVG, AwsSVG]

const BrandsShowcase = () => {
  return (
    <Showcase>
      {logos.map((Logo, index) => (
        <LogoContainer key={`logo-${index + 1}`}>
          <Logo />
        </LogoContainer>
      ))}
    </Showcase>
  )
}

const AboutPage = ({ data }) => {
  const matchMobile = useMediaQuery(`(min-width: ${TABLET_PORTRAIT_DEVICE}px)`)
  return (
    <AppLayout>
      <SEO title="About Us" canonical="about" />
      <HeadingContainer
        title="Build with your favorite Tech Stack"
        subtitle="Have a project in mind?"
      />
      <VideoContainer>
        <Iframe
          width="100%"
          height={!matchMobile ? '280' : '520'}
          src="https://www.youtube.com/embed/TBQazS1tyvM"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      </VideoContainer>

      <Container image={data.mountainsImage.resize.src}>
        <TextLeft>
          We provide software development services for complex software
          requirements using cutting edge technology for both open source
          (Javascript, Python) and .NET (C# primarily) and cloud services such
          as AWS, GCP and/or Azure.
        </TextLeft>
        <BrandsShowcase />
        <TextRight>
          We also focus on continuous training and selecting only the best staff
          for the jobs required in fields of AI, ML (Predictive Analytics), etc.
          This makes us a key player in the region (Latin America) and hopefully
          soon in the world as well.
        </TextRight>
      </Container>
    </AppLayout>
  )
}

export const query = graphql`
  query {
    mountainsImage: imageSharp(
      resize: { originalName: { eq: "mountains.png" } }
    ) {
      resize(jpegQuality: 100, quality: 100, width: 1200) {
        src
        originalName
      }
    }
  }
`

export default AboutPage
