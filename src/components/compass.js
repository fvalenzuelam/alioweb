/** @jsx jsx */
import { css, jsx } from '@emotion/react'
import styled from '@emotion/styled'
import Compasvg from '../images/compass/Compassvg2.inline.svg'
import bp from './common/breakpoints'

const Compass = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 320px;
  height: 320px;
  overflow: visible;
  z-index: 0;
  ${bp[0]} {
    width: 560px;
    height: 560px;
  }
`

const svgStyles = (props) =>
  css(`
  overflow: visible;
  z-index: 10;
  max-width: ${props.mobileMaxSize}px;
  max-height: ${props.mobileMaxSize}px;

  ${bp[0]} {
    max-width: ${props.maxSize}px;
    max-height: ${props.maxSize}px;
  }
  ${bp[2]} {
    max-width: 300px;
    max-height: 300px;
  }
  ${bp[3]} {
    max-width: 300px;
    max-height: 300px;
  } 
  @media (max-width: 1440px) {
    width: 380px;
    height: 380px;
  }
  ${bp[4]} {
    max-width: 500px;
    max-height: 500px;
  }
  #EstelaExterior {
    animation: agrandar 5s ease-out infinite;
  }
  #EstelaInterior {
    animation: agrandarInterior 5s 10s ease-out infinite;
  }
  #Navegacion {
    animation: girar 30s ease-in-out infinite;
  }
  #OrbitaBlanca {
    animation: girarAgrandar 10s ease-in-out infinite;
  }
  #OrbitaNaranja {
    animation: girarNaranja 10s ease-in-out infinite;
  }
  @keyframes agrandar {
    0% {
      transform-origin: center;
      transform:  scale(1);
      opacity: 0.4;
    }
    50% {
      transform-origin: center;
      transform: scale(2) rotate(360deg);
      opacity: 0;
    }
    100% {
      transform-origin: center;
      transform: scale(1) rotate(360deg);
      opacity: 0.4;
    }
  }

  @keyframes agrandarInterior {
    0% {
      transform-origin: center;
      transform:  scale(1);
      opacity: 0.4;
    }
    50% {
      transform-origin: center;
      transform: scale(1.6) rotate(180deg);
      opacity: 1;
    }
    60% {
      transform-origin: center;
      transform: scale(1.6) rotate(360deg);
      opacity: 1;
    }
    100% {
      transform-origin: center;
      transform: scale(1) rotate(180deg);
      opacity: 0.5;
    }
  }

  @keyframes girar {
    0% {
      transform-origin: center;
      transform:  rotate(0deg);
    }
    50% {
      transform-origin: center;
      transform:  rotate(180deg);
    }
    100% {
      transform-origin: center;
      transform:  rotate(0deg);
    }
  }
  @keyframes girarAgrandar {
    0% {
      transform-origin: center;
      transform:  scale(1) rotate(0deg);
    }
    50% {
      transform-origin: center;
      transform:  scale(1) rotate(180deg);
    }
    80% {
      transform-origin: center;
      transform:  scale(1.8) rotate(260deg);
    }
    100% {
      transform-origin: center;
      transform:  scale(1) rotate(0deg);
    }
  }
  @keyframes girarNaranja {
    0% {
      transform-origin: center;
      transform:  scale(1) rotate(0deg);
    }
    50% {
      transform-origin: center;
      transform:  scale(1) rotate(180deg);
    }
    80% {
      transform-origin: center;
      transform:  scale(1.6) rotate(260deg);
    }
    100% {
      transform-origin: center;
      transform:  scale(1) rotate(0deg);
    }
  }
  
`)
const CompassComponent = ({ ishomepage = false }) => {
  return (
    <Compass ishomepage={ishomepage}>
      <Compasvg
        css={svgStyles({
          mobileMaxSize: 310,
        })}
      />
    </Compass>
  )
}

export default CompassComponent
