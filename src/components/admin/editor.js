import React from 'react'
import styled from '@emotion/styled'

import { uploadAwsImage } from '../../utils/requests'

const Wrapper = styled.div`
  background-color: #ffffff;
  border: 1px;
  border-color: rgba(208, 207, 207, 0.4); 
  border-style: solid;
  border-radius: 5px;
  margin-bottom: 100px;
`

const EditorComponent = ({ editorRef, data = null }) => {
  const EditorJs = require('react-editor-js').default

  const TOOLS = {
    embed: require('@editorjs/embed'),
    table: require('@editorjs/table'),
    marker: require('@editorjs/marker'),
    list: require('@editorjs/list'),
    warning: require('@editorjs/warning'),
    code: require('@editorjs/code'),
    linkTool: require('@editorjs/link'),
    image: {
      class: require('@editorjs/image'),
      config: {
        uploader: {
          uploadByFile: async (file) => {
            const response = await uploadAwsImage(file)
            return {
              ...response,
              success: response.success ? 1 : 0,
            }
          },
        },
      },
    },
    raw: require('@editorjs/raw'),
    header: require('@editorjs/header'),
    quote: require('@editorjs/quote'),
    checklist: require('@editorjs/checklist'),
    delimiter: require('@editorjs/delimiter'),
    inlineCode: require('@editorjs/inline-code'),
  }

  return (
    <EditorJs
      enableReInitialize
      instanceRef={(ref) => (editorRef.current = ref)}
      tools={TOOLS}
      data={data}
    />
  )
}

const Editor = ({ editorRef, data = null }) => {
  return (
    <Wrapper>
      {typeof window !== `undefined` && (
        <EditorComponent editorRef={editorRef} data={data} />
      )}
    </Wrapper>
  )
}

export default Editor
