import React from 'react';
import styled from '@emotion/styled'
import GitdoneSVG from '../images/icons/solutions/Gitdone.svg'
import { FiArrowRightCircle } from "react-icons/fi";

const NewProduct = () => {
  return (
    <Container>
      <Content>
          <GitdoneSVG style={{width: "40%"}} />
          <Link href="https://www.gitdone.io/">
            <Font>Try it for free</Font>
            <FiArrowRightCircle style={{fontSize: "32px", color: "#ea8d22"}} />
          </Link>
      </Content>
    </Container>
  )
}

const Container = styled.div`
  padding: 8px 16px;
  position: relative;
  background: white;
  z-index: 1;
  display: flex;
  flex-direction: column;
  align-items: left;
  border-radius: 10px;
  max-width: 350px;
  transition: 100ms ease-out;
  &:hover {
    color: #ea8d22;
    opacity: 1;
    border-left: 3px solid #ea8d22;
  }
  @media (max-width: 812px) {
    max-width: 100%;
  }
`

const Content = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
`
const Link = styled.a`
  display: flex;
  cursor: pointer;
  justify-content: center;
  align-items: center;
  &:hover {
    color: #ea8d22;
    font-weight: 700;
  }
`

const Font = styled.p`
  font-size: 1.2rem;
  color: black;
  margin-right: 1rem;
  opacity: 0.6;
  font-weight: 300;
  &:hover {
    color: #ea8d22;
    opacity: 1;
  }
`
const Font2 = styled.p`
  position: absolute;
  top: 0;
  left: 0;
  font-size: .4rem;
  color: white;
  background-color: #ea8d22;
  border-radius: 100px;
  padding: 8px;
  margin: 0;
  @media (max-width: 812px) {
    margin-right: 1rem;
  }
`

export default NewProduct