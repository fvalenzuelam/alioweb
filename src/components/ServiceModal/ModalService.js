import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import './modalService.css'


const ModalService = (props)=> {
    const {item, show, setShow} = props;
    const handleClose = () => setShow(false);

    return (
        <>
          <Modal show={show} onHide={handleClose} className="special_modal" size="xl">
            <Modal.Header>
                <Modal.Title>{item.title}</Modal.Title>
            </Modal.Header>
                <Modal.Body>
                <div dangerouslySetInnerHTML={{__html: item.longdescription}} />
                </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose} className='modal-button'>
                Close
                </Button>
            </Modal.Footer>
          </Modal>
        </>
    );
}

export default ModalService;