import React, { useState, useEffect } from 'react'
import styled from '@emotion/styled'
import { Text } from '../components/common/typography'
import bp from '../components/common/breakpoints'
import { BLACK_COLOR, PRIMARY_COLOR, WHITE_COLOR } from './common/color'
import { motion, useAnimation } from 'framer-motion'
import { useInView } from 'react-intersection-observer'
import ModalService from './ServiceModal/ModalService'
import {
  STRATEGICCONSULTING,
  STARTUP,
  SOFTWAREDEVELOPMENT,
  MINING,
  CREATIVE,
  SALESFORCE,
  UIUX,
} from './common/images'

const services = [
  {
    number: '01',
    title: 'Strategic Consulting',
    description:
      'We help you identify opportunities for improvement and with our engineering expertise help come up with tailor made solutions.',
    longdescription: `Strategic consulting is a type of consulting that helps organizations improve their overall performance and achieve their goals by identifying areas of improvement and creating custom solutions to address them. These improvements can range from increasing efficiency and productivity to improving customer satisfaction and boosting revenue.<br/><br/>At the heart of strategic consulting is the idea of analyzing an organization\'s current operations, processes, and systems to identify areas of weakness and inefficiency. Once these areas are identified, strategic consultants can use their expertise and knowledge to develop custom solutions that address these issues and help the organization achieve its goals.<br/><br/> One of the key benefits of strategic consulting is the ability to tailor solutions to meet the unique needs of each organization. Because every organization is different, there is no one-size-fits-all solution to improving performance. Strategic consultants work closely with clients to understand their specific goals, challenges, and constraints, and use this information to develop solutions that are tailored to their needs.<br/><br/><img src=${STRATEGICCONSULTING} alt="Strategic Consulting" style="width: 100%; height: auto;"><br/><br/>In the context of engineering, strategic consulting can help organizations optimize their processes and systems, improve their product development cycles, and reduce costs by identifying areas where technology can be leveraged to streamline operations. For example, a strategic consultant might work with an organization to identify opportunities to automate repetitive tasks, or to use data analytics to gain insights into customer behavior and preferences.<br/><br/>Ultimately, the goal of strategic consulting is to help organizations achieve their full potential by identifying areas for improvement and developing custom solutions that address their specific needs. By leveraging the expertise of strategic consultants with engineering backgrounds, organizations can optimize their operations and achieve their goals more efficiently and effectively than they would be able to on their own.`,
    path: 'consulting',
  },
  {
    number: '02',
    title: 'Start Up Tech Partner',
    description:
      'Let us be part of the Magic! We can augment your Tech staff be it Software Devs, QA Devs, DevOps,etc to help you build the future.',
    longdescription: `As a startup tech partner, one of the main things we offer is the ability to augment your existing tech staff with additional resources that can help you build the future. Whether you need additional software developers to help you build your product, QA developers to ensure that your product is high-quality and bug-free, or DevOps professionals to help you deploy and manage your infrastructure, we can provide the skilled resources you need to achieve your goals. <br/><br/>By augmenting your tech staff with additional resources, you can benefit in a number of ways. First, you can accelerate the pace of development, enabling you to bring your product to market more quickly. With additional software developers, for example, you can work on multiple features or components of your product simultaneously, reducing development time and getting your product into the hands of your customers faster.<br/><br/>Second, by augmenting your tech staff with additional resources, you can increase the quality and reliability of your product. With additional QA developers, for example, you can ensure that your product is thoroughly tested and free of bugs before it is released to the public, reducing the risk of negative customer experiences or reputational damage.<br/><br/><img src=${STARTUP} alt="Strategic Consulting" style="width: 100%; height: auto;"><br/><br/>Finally, by augmenting your tech staff with additional resources, you can free up your existing staff to focus on higher-level tasks and strategic initiatives. With additional DevOps professionals, for example, you can offload the management of your infrastructure to experts, freeing up your existing staff to focus on product development or other strategic initiatives.Overall, as a startup tech partner, we can help you build the future by providing the skilled resources you need to accelerate development, increase quality and reliability, and free up your existing staff to focus on strategic initiatives. Whether you need additional software developers, QA developers, DevOps professionals, or other resources, we can provide the talent you need to achieve your goals and succeed in the highly competitive startup landscape.`,
    path: 'partner',
  },
  {
    number: '03',
    title: 'Software Development',
    description:
      'We build mobile, web, cloud apps for your business and guide you with our batch of experts to take the best and fastest path towards monetization for your application/business.',
    longdescription: `Software development consulting is a type of consulting that helps businesses build high-quality software applications that meet their unique needs and requirements. At its core, software development consulting is about helping businesses leverage technology to achieve their goals and stay ahead of the competition.    One of the key areas of focus for software development consulting is mobile, web, and cloud app development. Mobile, web, and cloud apps are critical tools for businesses in today\'s digital age, enabling them to connect with customers, streamline operations, and drive revenue growth. As a software development consulting firm, we specialize in building these types of apps for businesses of all sizes and across all industries.<br/><br/>   Our team of experts includes skilled software developers, designers, and project managers who work closely with clients to understand their unique needs and requirements. Using this information, we can develop custom mobile, web, and cloud apps that are tailored to our clients\' specific needs, preferences, and goals.    But building an app is only part of the equation. The real challenge for businesses is monetizing their apps quickly and effectively. As a software development consulting firm, we specialize not only in building apps, but also in guiding our clients towards the fastest path to monetization.<br/><br/><img src=${SOFTWAREDEVELOPMENT} alt="Strategic Consulting" style="width: 100%; height: auto;"><br/><br/>We use our expertise and knowledge of the market to help our clients make strategic decisions about app development and monetization. This might include identifying the most profitable revenue models, developing effective marketing strategies, or optimizing user engagement and retention.<br/><br/>    Ultimately, our goal as a software development consulting firm is to help businesses build high-quality mobile, web, and cloud apps that meet their unique needs and requirements, and to guide them towards the fastest and most effective path to monetization. By leveraging our team of experts and our deep understanding of the industry, we can help businesses succeed in the highly competitive digital landscape and achieve their goals.`,
    path: 'development',
  },
  {
    number: '04',
    title: 'Agile Project Management',
    description: 'We are fans of Agility and practice what we preach.',
    longdescription: `Agile project management is a methodology used in software development that emphasizes flexibility, collaboration, and iterative development. Instead of using a traditional, linear project management approach, Agile project management is based on the principles of the Agile Manifesto, which prioritize delivering value to customers, responding to change, and working collaboratively as a team.    The Agile approach involves breaking down a project into small, manageable chunks called sprints. Each sprint is a short period of time, typically two to four weeks, during which a team works to deliver a set of features or functionality that can be tested and evaluated by stakeholders. At the end of each sprint, the team reviews their progress, evaluates the feedback received, and adjusts their approach as necessary.<br/><br/>    The benefits of Agile project management include: <ol> <li><b>Increased flexibility</b>: Agile project management allows for changes to be made mid-project, based on feedback from stakeholders or changes in market conditions.</li> <li><b>Increased collaboration</b>: Agile project management emphasizes collaboration between team members, stakeholders, and customers, resulting in better communication and understanding of project requirements.</li> <li><b>Faster time to market</b>: Because Agile project management emphasizes iterative development and constant feedback, projects can be completed more quickly than traditional linear approaches.</li> <li><b>Improved quality</b>: The iterative nature of Agile project management allows for constant testing and evaluation, resulting in a higher-quality product.</li> <li><b>Better stakeholder satisfaction</b>: By involving stakeholders in the development process and prioritizing their feedback, Agile project management can result in a product that better meets their needs and expectations.</li> </ol> Overall, Agile project management is a highly effective methodology for software development projects, as it allows for flexibility, collaboration, and iterative development, resulting in faster time to market, improved quality, and better stakeholder satisfaction.`,
    path: 'agile',
  },
  {
    number: '05',
    title: 'Manufacturing and Mining Solutions',
    description: 'Bring your business to the next era in digitalization.',
    longdescription: `At Alio IT, we offer cutting-edge software solutions for manufacturing and mining operations. Our software is designed to optimize processes, increase efficiency, and reduce costs across a range of industries, from automotive to mining to oil and gas. <br/><br/>    Our manufacturing solutions include software for inventory management, production planning and scheduling, quality control, and supply chain management. Our software can help you streamline your operations, reduce waste, and improve product quality, while giving you greater visibility and control over your processes. <br/><br/>    Our mining solutions include software for exploration and geology, mine planning and scheduling, fleet management, and asset tracking. Our software can help you optimize your mining operations, reduce downtime, and improve safety, while giving you greater insights and control over your assets and resources. <br/><br/>    In addition to our software solutions, we offer exceptional customer support and training to ensure that you get the most out of our products. Our team of experts is always available to provide guidance and support, and we are dedicated to building long-term relationships with our clients based on trust and mutual success. <br/><br/>    At Alio IT, we are committed to helping our clients achieve their goals through innovative software solutions that drive success and growth.<br/><br/><img src=${MINING} alt="Strategic Consulting" style="width: 100%; height: auto;">`,
    path: 'mining',
  },
  {
    number: '06',
    title: 'Creative Solutions',
    description:
      'From UI design, graphics, animation, AR/VR, we use our skills to fulfill your business vision to attract more customers/business.',
    longdescription: `Creative solutions practice is all about using design and creativity to solve business problems and achieve business objectives. This can range from creating stunning graphics and animations for a website or social media campaign to developing immersive AR/VR experiences that engage and captivate customers. <br/><br/>    As a creative solutions firm, our team of experts specializes in a wide range of creative disciplines, including UI design, graphics, animation, and AR/VR development. We work closely with clients to understand their unique needs and goals, and use our skills and expertise to develop tailor-made solutions that help them achieve their vision and attract more customers and business.<br/><br/><img src=${CREATIVE} alt="Strategic Consulting" style="width: 100%; height: auto;"><br/><br/> One of the key areas of focus for our creative solutions practice is UI design. UI design is critical for creating a positive user experience on websites, apps, and other digital platforms. Our team of UI designers has a deep understanding of user behavior and user needs, and uses this knowledge to create intuitive and user-friendly interfaces that are optimized for engagement and conversion.<br/><br/>    Graphics and animation are also important components of our creative solutions practice. We specialize in creating stunning visuals and animations that help businesses stand out in a crowded digital landscape. Whether it\'s developing custom infographics for a blog post or creating a captivating video ad for a social media campaign, our team of graphic designers and animators has the skills and expertise to deliver high-quality results that drive engagement and conversions.<br/><br/>    Finally, our AR/VR development services are designed to create immersive, interactive experiences that engage and captivate users. Whether it\'s developing a virtual product demo or creating an AR-powered scavenger hunt, our team of AR/VR experts can help businesses create unique and memorable experiences that leave a lasting impression on customers and drive business growth.<br/><br/>    Overall, our creative solutions practice is focused on helping businesses achieve their goals by using design and creativity to solve problems and attract more customers and business. With our range of skills and expertise, we can develop tailor-made solutions that are optimized for engagement, conversion, and growth.`,
    path: 'solutions',
  },
  {
    number: '07',
    title: 'Salesforce Consulting',
    description:
      'Our team of certified Salesforce administrators, developers, and experienced project management (SCRUM) and QA process teams can be part of your team!',
    longdescription: `Salesforce is a powerful platform for managing customer relationships, streamlining business processes, and driving growth. However, to fully realize the benefits of Salesforce, it\'s important to have a team of experts who can help you customize and optimize the platform to meet your unique business needs.<br/><br/><img src=${SALESFORCE} alt="Strategic Consulting" style="width: 100%; height: auto;"> <br/><br/>As a Salesforce consulting firm, our team of certified administrators and developers has extensive experience working with clients across a range of industries to implement and customize Salesforce solutions. We work closely with clients to understand their business requirements, and use our expertise to develop tailor-made solutions that meet their needs and drive growth. <br/><br/>    Our team of Salesforce administrators has deep knowledge of the platform and its capabilities, and can help clients configure and customize Salesforce to meet their unique business needs. We can also provide ongoing support and maintenance to ensure that Salesforce is always running smoothly and delivering value to the business. <br/><br/>    Our team of Salesforce developers specializes in custom development on the platform, and can help clients build custom apps, integrations, and workflows that extend the capabilities of Salesforce and improve business processes. We also have experience working with a range of Salesforce products, including Sales Cloud, Service Cloud, Marketing Cloud, and Commerce Cloud, and can help clients select and implement the right products for their needs. <br/><br/>    In addition to our Salesforce administrators and developers, we also have experienced project management and QA process teams who can help clients manage their Salesforce projects from start to finish. We use SCRUM methodology to manage projects in an Agile way, ensuring that clients have visibility into the development process and can provide feedback throughout. Our QA process teams ensure that all solutions are thoroughly tested and meet the highest standards of quality. <br/><br/>    Overall, our Salesforce consulting practice is designed to help businesses fully realize the benefits of Salesforce by providing expert guidance and support. Whether you need help configuring and customizing Salesforce, developing custom solutions, or managing a Salesforce project from start to finish, our team of certified Salesforce administrators, developers, and experienced project management and QA process teams can be part of your team and help drive growth for your business.`,
    path: 'salesforce',
  },
  {
    number: '08',
    title: 'UX/UI Design',
    description:
      'Understanding your Customer Journey is essential to defining the desired Flow and User Experience',
    longdescription: `UX/UI design is the process of creating the visual and interactive elements of a digital product, such as a website or mobile application, to optimize the user experience. A great UX/UI design ensures that the product is intuitive and easy to use, while also creating an aesthetically pleasing and engaging interface that encourages users to engage with the product. One of the key goals of UX/UI design is to create a seamless and enjoyable user experience that helps users achieve their goals and accomplish their tasks quickly and easily. This involves understanding user behavior and needs, and using this information to design an interface that is intuitive and easy to use. <br/><br/>    To achieve this, UX/UI designers work closely with clients to understand their business goals and objectives, as well as their target audience and user needs. They conduct research, such as user testing and interviews, to gain insights into user behavior and preferences, and use this information to inform the design process. <br/><br/>    UX/UI design also involves creating a visual design that is aesthetically pleasing and engaging for users. This includes selecting the right color schemes, typography, and imagery to create a visually appealing interface that encourages users to engage with the product. <br/><br/><img src=${UIUX} alt="Strategic Consulting" style="width: 100%; height: auto;"><br/><br/> Overall, UX/UI design is critical for creating a positive user experience that encourages engagement and drives business growth. A well-designed user interface can help users achieve their goals quickly and easily, while also creating a memorable and enjoyable experience that encourages them to return to the product. If you\'re looking to optimize the user experience of your digital product, working with a UX/UI design team can help ensure that your product is intuitive, engaging, and optimized for success.`,
    path: 'design',
  },
  {
    number: '09',
    title: 'Web3 Development',
    description:
      'We have a team of Blockchain Developers focusing on Web3 with the following set of tools: Solidity for Smart contracts, OpenZepellin, PolygonSDK, Ether.js for front end and more.',
    longdescription: `Web3 development is the development of decentralized applications (dApps) using blockchain technology, typically built on the Ethereum blockchain. In Web3 development, the focus is on building applications that enable users to interact with decentralized networks, where data and transactions are stored on a distributed ledger rather than a centralized server.    One of the key features of Web3 development is the use of smart contracts, which are self-executing contracts with the terms of the agreement between buyer and seller being directly written into code. This enables secure and transparent transactions without the need for a centralized intermediary. <br/><br/>    Web3 development also involves the use of decentralized storage and computing resources, such as the InterPlanetary File System (IPFS) and the Ethereum Virtual Machine (EVM), which enable the creation of decentralized and censorship-resistant applications. <br/><br/>    Web3 development has the potential to revolutionize the way we interact with the internet, by enabling peer-to-peer interactions and creating decentralized and secure networks that are not controlled by any single entity. This has numerous implications, ranging from the creation of decentralized marketplaces and social networks to the development of new financial systems and voting mechanisms. <br/><br/>    Some of the key technologies and platforms used in Web3 development include Ethereum, Solidity (the programming language used to write smart contracts), IPFS, and various development frameworks such as Truffle and Embark. <br/><br/>    Overall, Web3 development represents an exciting new frontier in the world of technology, with the potential to transform the way we interact with the internet and create new decentralized systems and applications that are more secure, transparent, and democratic. If you\'re interested in exploring the world of Web3 development, there are numerous resources and communities available to help you get started.   <br/> <br/>Check out our Web3 product https: <a href="https://sbn.finance" target="_blank">https://sbn.finance !</a>`,
    path: 'web3',
  },
]

const Wrapper = styled(motion.div)`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  width: 100%;
  margin: 0 auto;
  box-sizing: border-box;
  background-color: transparent;
  ${bp[3]} {
  }
`

const Container = styled(motion.div)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  row-gap: 1.5rem;
  column-gap: 1.5rem;
  align-items: center;
  justify-items: center;
  width: 100%;
  margin: 0 auto;
  padding-bottom: 4rem;

  ${bp[1]} {
    grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
  }
`

const Card = styled(motion.div)`
  display: flex;
  flex-direction: column;
  gap: 1rem;
  max-width: 100%;
  min-height: 330px;
  width: 100%;
  padding: 2rem;
  background: ${BLACK_COLOR};
  box-sizing: border-box;
  border-radius: 4px;
  height: fit-content;
  position: relative;
  ${bp[2]} {
    grid-template-rows: 2fr auto 3fr;
    min-width: unset;
    // min-height: 40vh;
    // height: 40vh;
  }
  @media (max-width: 767px) {
    min-height: auto;
  }
`
const CardHeader = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0px;
  ${bp[2]} {
    display: flex;
    flex-direction: column;
    gap: 0px;
  }
`
const CardNumber = styled.h4`
  margin: 0;
  font-family: 'Manrope';
  text-transform: uppercase;
  font-weight: 700;
  font-size: 2rem;
  color: ${PRIMARY_COLOR};
  opacity: 0.8;
`
const CardTitle = styled.h4`
  margin: 0;
  font-family: 'source code pro';
  text-transform: uppercase;
  font-weight: 800;
  font-size: 2rem;
  color: ${WHITE_COLOR};
  opacity: 0.8;
`

const CardDescription = styled(Text)`
  font-weight: 400;
  color: ${WHITE_COLOR};
  opacity: 0.4;
  font-size: 1rem;
  display: none;
  font-family: 'Manrope';
  margin-bottom: 10%;
  ${bp[1]} {
    font-size: 0.75rem;
    display: unset;
  }
`
const HeadingWhite = styled(motion.h1)`
  font-size: 2.5rem;
  width: 100%;
  font-family: 'Manrope' !important;
  color: ${BLACK_COLOR};
  font-weight: 700;
  margin-bottom: 3rem;
  @media (max-width: 812px) {
    font-size: 2.5rem;
    max-width: unset;
    width: 100%;
  }
  @media (max-width: 576px) {
    font-size: 1.8rem;
  }
  ${bp[3]} {
    font-size: 4rem;
  }
`
const CareerDetailsContainer = styled.div`
  display: grid;
  grid-auto-flow: column;
`
export const ButtonNoStyle = styled.button`
  display: inline-flex;
  align-items: center;
  margin: 0;
  padding: 0.5rem;
  background-color: transparent;
  border: none;
  font-family: inherit;
  font-weight: 700;
  cursor: pointer;
  text-align: center;
  font-size: 1rem;
  color: ${PRIMARY_COLOR};
  position: absolute;
  bottom: 20px;
  ${bp[2]} {
    font-size: 12px;
    padding: 0px 16px;
  }
  &:focus {
    outline: none;
  }

  &:disabled {
    opacity: 0.6;
    cursor: not-allowed;
  }
`
const CardAnimate = {
  offscreen: { x: -10, opacity: 0 },
  onscreen: {
    x: 0,
    opacity: 1,
    transition: {
      type: 'spring',
      duration: 1,
    },
  },
}

const ServicesLanding = () => {
  const controls = useAnimation()
  const [ref, inView] = useInView()
  const [show, setShow] = useState(false)
  const [selectedItem, setSelectedItem] = useState(null)

  const handleItemClick = (item) => {
    setSelectedItem(item)
    setShow(true)
  }

  useEffect(() => {
    if (inView) {
      controls.start('onscreen')
    }
  }, [controls, inView])
  return (
    <Wrapper id="services">
      {selectedItem && (
        <ModalService item={selectedItem} show={show} setShow={setShow} />
      )}

      <HeadingWhite
        variants={CardAnimate}
        initial={'offscreen'}
        ref={ref}
        animate={controls}
        className='typewriter4'
      >
        Have a project in mind?
      </HeadingWhite>
      <Container
        initial={'offscreen'}
        ref={ref}
        animate={controls}
        transition={{ staggerChildren: 0.1 }}
      >
        {services.map((service) => (
          <Card
            variants={CardAnimate}
            key={service.title.toLocaleLowerCase().replace(' ', '-')}
          >
            <CardHeader>
              <CardNumber>{service.number}</CardNumber>
              <CardTitle>{service.title}</CardTitle>
            </CardHeader>
            <CardDescription>{service.description}</CardDescription>
            <CareerDetailsContainer>
              <ButtonNoStyle onClick={() => handleItemClick(service)}>
                Keep Reading
              </ButtonNoStyle>
            </CareerDetailsContainer>
          </Card>
        ))}
      </Container>
    </Wrapper>
  )
}

export default ServicesLanding