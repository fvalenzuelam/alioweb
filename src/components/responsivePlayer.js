import React, { useEffect, useRef } from 'react'
import styled from '@emotion/styled'
import ReactPlayer from 'react-player/lazy'
import AlioVideo from '../videos/alio.mp4'

const PlayerWrapper = styled.div`
  position: relative;
  height: 100vh;
  @media (max-width: 768px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 0%;
  }
`

const ResponsivePlayer = ({ controls, onProgress }) => {
  const videoRef = useRef(undefined)
  useEffect(() => {
    videoRef.current.defaultMuted = true
  })
  return (
    <PlayerWrapper>
      <ReactPlayer
        style={{ backgroundSize: 'cover' }}
        ref={videoRef}
        playing
        url={AlioVideo}
        muted
        width="100%"
        height="100%"
        controls={controls}
        onProgress={onProgress}
        volume="0.6"
      />
    </PlayerWrapper>
  )
}

export default ResponsivePlayer
