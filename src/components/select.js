import React from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import { PRIMARY_COLOR, BLACK_COLOR_RGB, WHITE_COLOR, BLACK_COLOR } from './common/color'
import InputBase from '@material-ui/core/InputBase'
import FormHelperText from '@material-ui/core/FormHelperText'

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(0.5),
    minWidth: 300,
  },
}))

const CustomInput = withStyles((theme) => ({
  root: {
    'label + &': {
      color: `${WHITE_COLOR}`,
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 0,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderRadius: 0,
      borderColor: PRIMARY_COLOR,
      boxShadow: '0 0 0 0.2rem rgba(255, 165, 0,.25)',
    },
  },
}))(InputBase)

export default function FilterSelect(props) {
  const { title, blogs, setFilter, setAuthorFilter } = props
  const classes = useStyles()
  const [open, setOpen] = React.useState(false)
  const [option, setOption] = React.useState('')

  const authors = blogs.map((element) => ({
    author: element.author,
  }))

  const topics = blogs.map((element) => {
    return element.topic
  })

  const topicsClean = topics.filter((item) => item)

  const topicsFilter = Object.values(
    topicsClean.reduce((acc, cur) => Object.assign(acc, { [cur]: cur }), {})
  )

  const authorFilter = Object.values(
    authors.reduce((acc, cur) => Object.assign(acc, { [cur.author]: cur }), {})
  )

  const handleChange = (event) => {
    setOption(event.target.value || '')
  }

  const handleClean = () => {
    setFilter('')
    setOption('')
    setAuthorFilter(null)
  }

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleOk = () => {
    title === 'Author' ? setAuthorFilter(true) : setAuthorFilter(false)
    setFilter(option)
    setOpen(false)
  }

  return (
    <div>
      <Button
        style={{
          fontSize: '12px',
          fontWeight: '600',
          color: `${BLACK_COLOR}`,
          background: `${WHITE_COLOR}`,
          fontFamily: 'Source Code Pro'
        }}
        onClick={handleClickOpen}
      >{`Filter By ${title}`}</Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle
          style={{ color: `${BLACK_COLOR_RGB(0.6)}`, paddingBottom: '0px' }}
        >{`Filter By ${title}`}</DialogTitle>
        <DialogContent>
          <form className={classes.container}>
            <FormControl className={classes.formControl}>
              <InputLabel
                style={{ color: PRIMARY_COLOR }}
                htmlFor="demo-dialog-native"
              >{`${title}`}</InputLabel>
              <Select
                labelId="demo-dialog-select-helper-label"
                id="demo-dialog-select-helper"
                value={option}
                onChange={handleChange}
                input={<CustomInput />}
              >
                {' '}
                {title === 'Author' &&
                  authorFilter.map((item) => {
                    return (
                      <MenuItem
                        style={{ color: `${BLACK_COLOR_RGB(0.4)}` }}
                        value={item.author}
                      >
                        {item.author}
                      </MenuItem>
                    )
                  })}
                {title === 'Topic' &&
                  topicsFilter.map((item) => {
                    return (
                      <MenuItem
                        style={{ color: `${BLACK_COLOR_RGB(0.4)}` }}
                        value={item}
                      >
                        {item}
                      </MenuItem>
                    )
                  })}
              </Select>
              <FormHelperText>Select an option</FormHelperText>
            </FormControl>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClean} style={{ color: PRIMARY_COLOR }}>
            Clean
          </Button>
          <Button onClick={handleClose} style={{ color: PRIMARY_COLOR }}>
            Cancel
          </Button>
          <Button onClick={handleOk} style={{ color: PRIMARY_COLOR }}>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
