import styled from '@emotion/styled'
import { css } from '@emotion/react'

const common = css`
  padding: 0.5rem;
  font-size: 1rem;
  font-weight: normal;
  font-family: var(--primary-font);
  background-color: #ffffff;
  border: 1px solid rgba(208, 207, 207, 0.4);
  border-radius: 5px;
  box-sizing: border-box;
`

export const Input = styled.input`
  ${common}
`

export const TextArea = styled.textarea`
  ${common}
  resize: none;
`

export const Label = styled.label`
  display: grid;
  row-gap: 0.25rem;
  font-weight: bold;
  font-size: 0.975rem;
`
