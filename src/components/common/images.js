import strategicConsulting from '../../images/strategic-consulting.png'
import startup from '../../images/startup.png'
import softwaredev from '../../images/softwaredev.jpg'
import mining from '../../images/mining.png'
import creative from '../../images/creative.png'
import salesforce from '../../images/salesforce.png'
import uiux from '../../images/uiux.png'
import mountains from '../../images/mountains.png'
import googleSignin from '../../images/google-signin.png'


export const STRATEGICCONSULTING = strategicConsulting
export const STARTUP = startup
export const SOFTWAREDEVELOPMENT = softwaredev
export const MINING = mining
export const CREATIVE = creative
export const SALESFORCE = salesforce
export const UIUX = uiux
export const MOUNTAINS = mountains
export const GOOGLESIGNIN = googleSignin
