import React from 'react';
import styled from '@emotion/styled'
import Lottie from 'react-lottie'
//import animationData from './lotties/ethAnimation'
import animationData from './lotties/catAvatar'
import { BLACK_COLOR } from './common/color'
import bp from './common/breakpoints'
import { motion } from 'framer-motion'
import { MainButton } from './common/button';
import Cooldevs from '../images/assets/cooldevs-logo.svg'

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice"
  }
};

const HeadingWhite = styled(motion.h1)`
  font-size: 2.5rem;
  width: 100%;
  text-align: left;
  line-height: 1.50;
  font-family: 'Manrope' !important;
  color: ${BLACK_COLOR};
  font-weight: 700;
  margin-bottom: 3rem;
  @media (max-width: 812px) {
    font-size: 2.5rem;
    max-width: unset;
    width: 100%;
  }
  @media (max-width: 576px) {
    font-size: 1.8rem;
  }
  ${bp[3]} {
    font-size: 4rem;
    line-height: 1.8;
  }
`
const ContentContainer = styled.div`
display: grid;

${bp[3]} {
  grid-template-columns: 1fr 2fr; 
  }
`
const LottieContainer = styled.div`
display: flex;
padding-bottom: 10px;
`
const ButtonContainer = styled.div`
display: grid;
grid-template-rows: 2fr;
`
const buttonStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}
const lottieStyle = {
  overflow: 'hidden',
  borderRadius: '8px'
}
const ImgContainer = styled.div`
display:none;
align-items: center;
justify-content: center;

${bp[3]} {
  display:flex;
  }
`
const NftInfo = () => {


  return (
    <div>
      <HeadingWhite className='typewriter4'>Check out our <span style={{ color: '#EF761F' }}> NFTs </span> </HeadingWhite>
      <ContentContainer>
        <LottieContainer>
          <Lottie
            options={defaultOptions}
            height={250}
            width={250}
            style={lottieStyle}
          />
        </LottieContainer>
        <ButtonContainer>
          <ImgContainer>
            <img src={Cooldevs} alt="" />
          </ImgContainer>
          <MainButton
            to='https://nft.alioit.com/'
            icon={'trending-up'}
            style={buttonStyle}>
            Find out more
          </MainButton>
        </ButtonContainer>
      </ContentContainer>
    </div >
  );
}
export default NftInfo