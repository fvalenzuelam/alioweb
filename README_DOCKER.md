## Instructions for docker setup on windows

In order to run the project on a windows machine you must use a docker instance running linux due to a bug regarding image encoding and display.

---
### Prepwork & Dockerfiles 

In order tu succesfully run the project you must make sure that the files <mark>Dockerfile</mark> and <mark>docker-compose.yml</mark> exist in the project root (*same path as this readme*).


![](2023-01-24-11-21-25.png)

you must also make sure that <mark>gatbsy-node.js</mark> has this at the end:
```
exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    watchOptions: {
      aggregateTimeout: 200,
      poll: 1000,
    },
  })
}
```

---
## If the files are somehow lost or deleted:

### Dockerfile (no extension needed)
```
FROM node:16-bullseye
EXPOSE 8000

RUN npm config set unsafe-perm true
RUN npm install -g gatsby-cli@4.19.0
WORKDIR /app
COPY ./package.json /app
RUN npm cache clean --force
RUN npm install --arch=x64 --platform=linux --legacy-peer-deps
COPY . /app

RUN mkdir node_modules/.cache && chmod -R 777 node_modules/.cache
CMD ["gatsby", "develop", "-H", "0.0.0.0" ]
```
### docker-compose.yml
```
version: "2.4"
services:
  web:
    platform: linux/amd64
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - "8000:8000"
    volumes:
      - /app/node_modules
      - .:/app
    environment:
      - NODE_ENV=development
```
---
## Running the Docker container

You will need have installed [Docker](https://www.docker.com/products/docker-desktop/) and [WSL](https://learn.microsoft.com/en-us/windows/wsl/install) in order to succesfully run the container. After installing you will then need to start the container in order to view the site locally. 



```
docker-compose up
```
Do note that this will take a while to run depending on your system specs (165s-240s on a Ryzen 5700x & 16GB ram). Access URL should be http://localhost:8000 Use CTRL + C to stop the container.

In order to relaunch the instance  with new changes if you're not using docker for anything else then run this (WARNING: this will DELETE all the existing docker containers, images and volumes).

```
docker system prune -a
```
If you're using Docker for something other than this project then manually delete the container, image and volume created for the website and compose it again. 


There probably is a way to implement hot-reloading, I didn't really research it that much. Make a trade union/syndicate. Good luck I hope you never ever have to read this.




