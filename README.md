# Alioit Website

This repo contains the business website for Alioit that is built with [Gatsby](https://www.gatsbyjs.org/), and [Netlify CMS](https://www.netlifycms.org)

It follows the [JAMstack architecture](https://jamstack.org) by using Git as a single source of truth, and [Netlify](https://www.netlify.com) for continuous deployment, and CDN distribution.

*it currently works with node version 8, problems using 12 and gatsby-plugin-sharp (nvm use 8.x.x)*

## Getting Started (Recommended)

The CMS can run in any frontend web environment.

Next, you’ll need to set up Netlify’s Identity service to authorize users to log in to the CMS.

### Access Locally
```
$ git clone git@gitlab.com:alioit/website.git alioit
$ cd alioit
$ yarn
$ npm run develop
```
To test the CMS locally, you'll need run a production build of the site:
```
$ npm run build
$ npm run serve
```