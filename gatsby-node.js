/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it
exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions

  const templates = {
    blog: require.resolve('./src/templates/blog-template.js'),
    careers: require.resolve('./src/templates/career-template.js'),
  }

  const result = await graphql(`
    {
      blogPosts: allMarkdownRemark(
        filter: { fields: { sourceInstanceName: { eq: "blog" } } }
      ) {
        edges {
          node {
            id
            fields {
              sourceInstanceName
            }
            parent {
              ... on File {
                id
                name
              }
            }
          }
        }
      }
      careers: allMarkdownRemark(
        filter: { fields: { sourceInstanceName: { eq: "careers" } } }
      ) {
        edges {
          node {
            id
            fields {
              sourceInstanceName
            }
            parent {
              ... on File {
                id
                name
              }
            }
          }
        }
      }
      careersOpportunity: allMarkdownRemark(
        filter: { fields: { sourceInstanceName: { eq: "careersOpportunity" } } }
      ) {
        edges {
          node {
            id
            fields {
              sourceInstanceName
            }
            parent {
              ... on File {
                id
                name
              }
            }
          }
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const {
    data: { blogPosts, careers, careersOpportunity },
  } = result
  blogPosts.edges.forEach(({ node: post }) => {
    const { sourceInstanceName } = post.fields
    createPage({
      path: `/${post.fields.sourceInstanceName}/${post.parent.name}`,
      component: templates[sourceInstanceName],
      context: {
        blogPostId: post.id,
      },
    })
  })

  careers.edges.forEach(({ node: career }) => {
    const { sourceInstanceName } = career.fields
    createPage({
      path: `/${career.fields.sourceInstanceName}/${career.parent.name}`,
      component: templates[sourceInstanceName],
      context: {
        careerId: career.id,
      },
    })
  })

  careersOpportunity.edges.forEach(({ node: post }) => {
    const { sourceInstanceName } = post.fields
    createPage({
      path: `/${post.fields.sourceInstanceName}/${post.parent.name}`,
      component: templates[sourceInstanceName],
      context: {
        blogPostId: post.id,
      },
    })
  })

  exports.onCreateWebpackConfig = ({ actions }) => {
    actions.setWebpackConfig({
      watchOptions: {
        aggregateTimeout: 200,
        poll: 1000,
      },
    })
  }
}
